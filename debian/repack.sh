#!/bin/sh

_usage()
{
    printf "usage: %s <ngrep-1.xx.tar.bz2>\n" `basename $0`
    exit 1
}

if [ $# -ne 1 ]; then
    _usage
else
    TARFILE="$1"
fi

DIR=$(tar tf $TARFILE | cut -d/ -f1 | tail -1)
tar xf $TARFILE
rm -rf $DIR/pcre-* $DIR/regex-* $DIR/win32
tar cvvzf $DIR.tar.gz $DIR
